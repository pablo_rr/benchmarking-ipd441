import os
import cv2

import pandas as pd
import numpy as np
import pathlib, os

import tensorflow as tf

from object_detection.utils import label_map_util
from object_detection.utils import config_util
from object_detection.utils import visualization_utils as viz_utils
from object_detection.builders import model_builder

# Obtención de cajas englobantes por contornos
def get_blobs(labels):
	bboxes = {}
	i, j = 0, 0
	
	while j < labels.shape[0]:
		i = 0
		while i < labels.shape[1]:
			label = labels[j][i]
			if label > 0:
				if label not in bboxes.keys():
					r = [i, j, 1, 1]
					bboxes[label] = r
				else:
					r = bboxes[label]
					x = r[0] + r[2] - 1
					y = r[1] + r[3] - 1
					if i < r[0]:
						r[0] = i 
					if i > x:
						x = i 
					if j < y:
						r[1] = j
					if j > y:
						y = j 
					r[2] = x - r[0] + 1
					r[3] = y - r[1] + 1
			i += 1
		j += 1
	return bboxes

# Se dibuja rectángulos a partir de cajas englobantes
def paint_rectangles(img, bboxes):
	for bbox in bboxes.values():
		start_point = (bbox[0], bbox[1])
		end_point = (bbox[0] + bbox[2], bbox[1] + bbox[3])
		img = cv2.rectangle(img, start_point, end_point, (0, 0, 255), 2)
			
	return img

# Obtención del modelo de detección. De RoboFlow tutorial
def get_model_detection_function(model):
	"""Get a tf.function for detection."""

	@tf.function
	def detect_fn(image):
		"""Detect objects in image."""

		image, shapes = model.preprocess(image)
		prediction_dict = model.predict(image, shapes)
		detections = model.postprocess(prediction_dict, shapes)

		return detections, prediction_dict, tf.reshape(shapes, [-1])

	return detect_fn

# Parámetros a, b de la matriz de proyección
def find_translation(angle, height=0.15, radius=0):
	a = height * np.cos(angle) + radius * np.sin(angle)
	b = height * np.sin(angle) - radius * np.cos(angle)
	return a, b

# Matriz de proyección ad-hoc para el problema
def p_matrix(angle, height, alpha, radius=0):
    a, b = find_translation(angle, height, radius)
    p = np.array([[112 * np.cos(angle), -alpha, 112 * b],
                  [112 * np.cos(angle) - alpha * np.sin(angle), 0, a * alpha + 112 * b],
                  [np.cos(angle), 0, b]])
    return p

# Parámetros simulados para la matriz RT e intrínseca
angle = 15 * np.pi / 180
height = 0.15
alpha = 150
radius = 0.09

# Matriz de proyección y homografía
p_mat = p_matrix(angle, height, alpha, radius)
h_mat = np.linalg.inv(p_mat) * np.linalg.det(p_mat)

# Selección de imágenes para inferencia y proyección
imgs_path = ['ssl-dataset-master/1_resized/{:05d}.jpg'.format(i) for i in range(817, 864 + 1)]
# imgs_path = ['ssl-dataset-master/1_resized/{:05d}.jpg'.format(i) for i in range(865, 930 + 1)]

# Se obtienen los archivos de checkpoints. De RoboFlow Tutorial
filenames = list(pathlib.Path('training/').glob('*.index'))
filenames.sort()

# Archivo de configuración de pipeline. De RoboFlow Tutorial
pipeline_config = 'training/ssd_efficientdet_d0.config'

# Se detecta de forma automática el último checkpoint del entrenamiento. De RoboFlow Tutorial
model_dir = str(filenames[-1]).replace('.index','')
configs = config_util.get_configs_from_pipeline_file(pipeline_config)
model_config = configs['model']
detection_model = model_builder.build(
	  model_config=model_config, is_training=False)

# Se restaura el último checkpoint
ckpt = tf.compat.v2.train.Checkpoint(model=detection_model)
ckpt.restore(os.path.join(str(filenames[-1]).replace('.index','')))

# Etiquetas para la decodificación al hacer inferencia. De RoboFlow Tutorial
label_map_path = configs['eval_input_config'].label_map_path
label_map = label_map_util.load_labelmap(label_map_path)
categories = label_map_util.convert_label_map_to_categories(
	label_map,
	max_num_classes=label_map_util.get_max_label_map_index(label_map),
	use_display_name=True)
category_index = label_map_util.create_category_index(categories)
label_map_dict = label_map_util.get_label_map_dict(label_map, use_display_name=True)

detect_fn = get_model_detection_function(detection_model)

# Almacenamiento de vídeo
save_video = False

if save_video:
	fourcc = cv2.VideoWriter_fourcc(*'mp4v')
	out = cv2.VideoWriter('pres/final/output.mp4', fourcc, 10.0, (448, 224))

# Datos de posición proyectada de objetos para gráficos posteriores
global_pos = []

# Se itera sobre el set de imágenes seleccionado
for i, f in enumerate(imgs_path):
	img = cv2.imread(f)
	
	# Se realiza la inferencia con la imagen cargada. En parte de RoboFlow Tutorial
	input_tensor = tf.convert_to_tensor(np.expand_dims(img, 0), dtype=tf.float32)
	detections, predictions_dict, shapes = detect_fn(input_tensor)

	label_id_offset = 1
	image_np_with_detections = img.copy()

	viz_utils.visualize_boxes_and_labels_on_image_array(
		image_np_with_detections,
		detections['detection_boxes'][0].numpy(),
		(detections['detection_classes'][0].numpy() + label_id_offset).astype(int),
		detections['detection_scores'][0].numpy(),
		category_index,
		use_normalized_coordinates=True,
		max_boxes_to_draw=200,
		min_score_thresh=.6,
		agnostic_mode=False,
	)

	# Se obtienen los scores, clases y cajas englobantes de la detección
	scores = detections['detection_scores'][0].numpy()
	classes = detections['detection_classes'][0].numpy()
	bboxes = detections['detection_boxes'][0].numpy() * 224 # anchos denormalizados

	# Se filtran las detecciones con fiabilidad > 0.6
	filter = scores > 0.6
	filt_classes = classes[filter]
	filt_boxes = bboxes[filter]

	# Función de comparación de las dimensiones de cajas englobantes para filtrar arcos pequeños
	def cmp_width_height(bbox):
		if (bbox[3] - bbox[1]) < 3 * (bbox[2] - bbox[0]):
			return False
		return True

	# Inicialización de variables
	detect = img.copy()
	goal_pos = None
	ball_pos = None

	# Se itera sobre detecciones para trabajar sobre arcos
	for b, c in zip(filt_boxes.astype(np.int), filt_classes):
		# Se revisa si la clase es 'goal' -> 0: ball, 1: goal, 2: robot
		# Se espera que la caja englobante sea 3 veces más ancha que alta, con el fin de detectar el centro del arco
		if c == 1 and cmp_width_height(b):
			# Se selecciona ROI del arco
			goal = img[b[0]:b[2], b[1]:b[3]]

			# Se carga mapa YUV, filtro gaussiano, bordes
			goal_yuv = cv2.cvtColor(goal, cv2.COLOR_BGR2YUV)			
			goal_filt = cv2.GaussianBlur(goal_yuv[:, :, 0], (3, 3), 0)
			canny = cv2.Canny(goal_filt, 50, 100)

			# Detección de líneas
			cdst = np.zeros(img.shape, np.uint8)
			lines = cv2.HoughLinesP(canny, 1, 0.5 * np.pi / 180, 50, None, 40, 200)

			# Se verifica si se han detectado líneas
			if lines is not None:
				# Base del arco
				base_line = []
				for line in lines:
					l = line[0]

					# Se agrega el punto x0, y0 de la caja englobante para ubicar en la imagen completa
					l[0] += b[1]
					l[1] += b[0]
					
					l[2] += b[1]
					l[3] += b[0]

					# Se selecciona la línea con mayores coordenadas y, asumiendo que son correctamente detectadas
					if len(base_line) == 0 or (l[1] > base_line[1] and l[3] > base_line[3]):
						base_line = (l[0], l[1], l[2], l[3])
					
					# Se dibuja la línea en cdst
					cv2.line(cdst, (l[0], l[1]), (l[2], l[3]), (0, 0, 255), 3, cv2.LINE_AA)

				# Se obtiene el punto central de la línea de la base del arco para establecer posición
				goal_pos = np.array([(base_line[2] - base_line[0]) / 2 + base_line[0],
									 (base_line[3] - base_line[1]) / 2 + base_line[1]]).astype(np.int)

			# Detector de esquinas del arco. Se implementa pero no se usa
			# dst = cv2.cornerHarris(cv2.cvtColor(cdst, cv2.COLOR_BGR2GRAY), 3, 5, 0.04)

			# Se marca la posición del arco en detect
			cv2.circle(detect, goal_pos, 2, (0, 0, 255), -1)   

			# Se dibujan las líneas del arco en detect.
			#img[dst > 0.1 * dst.max()] = [0, 0, 255]
			detect = cv2.bitwise_or(detect, cdst)

	# Detección de la pelota a través de V-U
	yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
	vu = (yuv[:, :, 2].astype(np.float32)) - yuv[:, :, 1].astype(np.float32)
	vu[vu < 0.0] = 0.0
	vu[vu > 255.0] = 255.0
	vu = vu.astype(np.uint8)
	_, vu_mask = cv2.threshold(vu, 30, 255, cv2.THRESH_BINARY)

	# Se obtienen contornos
	(contours, hierarchy) = cv2.findContours(vu_mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	
	# Se obtienen componentes conectados y cajas englobantes
	if len(contours) > 0:
		dest = np.zeros(vu_mask.shape, np.uint8)	
		cont = max(contours, key=lambda x: cv2.contourArea(x))
		dest = cv2.drawContours(dest, [cont], -1, (255), thickness=cv2.FILLED)

		num_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(dest)
		bbox = get_blobs(labels)

		dest = cv2.cvtColor(dest, cv2.COLOR_GRAY2BGR)

		# Podría ser más flexible. Por ahora se acepta solo 1 pelota
		ball_pos = [int(bbox[1][0] + bbox[1][2] / 2), int(bbox[1][1] + bbox[1][3])]
		# paint_rectangles(detect, bbox)

		# Se marca la posición de la pelota en la imagen
		cv2.circle(detect, ball_pos, 2, (255, 0, 0), -1)

	# En caso de que no se detecten objetos se almacena nan en el archivo para gráficos
	(xt_goal, yt_goal, t_goal) = (np.nan, np.nan, np.nan)
	(xt_ball, yt_ball, t_ball) = (np.nan, np.nan, np.nan)

	# Se imprime información del frame actual
	print('Frame {}'.format(i))

	# Si se detectó arco se almacena info y se dibuja
	if goal_pos is not None:
		# Distancia del robot al arco detectado
		pgoal = np.array([goal_pos[0], goal_pos[1], 1])
		(xt_goal, yt_goal, t_goal) = h_mat.dot(pgoal)

		cv2.line(detect, (112, 224), goal_pos, (0, 0, 255), 1)
		print('Goal pixel pos: {}, Goal pos r/robot: ({:.2f}, {:.2f})'.format(goal_pos, xt_goal / t_goal, yt_goal / t_goal))

	# Si se detectó pelota se almacena info y se dibuja
	if ball_pos is not None:
		# Distancia del robot a la pelota
		pball = np.array([ball_pos[0], ball_pos[1], 1])
		(xt_ball, yt_ball, t_ball) = h_mat.dot(pball)

		cv2.line(detect, (112, 224), ball_pos, (255, 0, 0), 1)
		print('Ball pixel pos: {}, Ball pos r/robot: ({:.2f}, {:.2f})'.format(ball_pos, xt_ball / t_ball, yt_ball / t_ball))

	# Se guarda información de posición de objetos
	global_pos.append([xt_goal / t_goal, yt_goal / t_goal, xt_ball / t_ball, yt_ball / t_ball])

	if goal_pos is None and ball_pos is None:
		print('No detection')
	print('\n')

	# Si se seteó guardado de vídeo, se escribe frame
	if save_video:
		frame = np.hstack((image_np_with_detections, detect))
		out.write(frame)

	# Se muestra detección por CNN y con algoritmo de proyección
	cv2.imshow('goal', detect)	
	cv2.imshow('Img', image_np_with_detections)

	# Salir si k es q o Esc. Guardar frame si k es s
	k = cv2.waitKey() & 0xFF
	if k == ord('q') or k == 27:
		break
	if k == ord('s'):
		cv2.imwrite('pres/final/{}_detection.png'.format(i), image_np_with_detections)
		cv2.imwrite('pres/final/{}_projection.png'.format(i), detect)

# Se exportan los datos de posición a csv
df = pd.DataFrame(np.array(global_pos), columns=['goal_xpos', 'goal_ypos', 'ball_xpos', 'ball_ypos'])
df.to_csv('pres/final/global_pos.csv')

# Se cierra el programa
if save_video:
	out.release()

cv2.destroyAllWindows()