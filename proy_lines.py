import os
import cv2
import numpy as np

# Obtención de cajas englobantes por contornos
def get_blobs(labels):
	bboxes = {}
	i, j = 0, 0
	
	while j < labels.shape[0]:
		i = 0
		while i < labels.shape[1]:
			label = labels[j][i]
			if label > 0:
				if label not in bboxes.keys():
					r = [i, j, 1, 1]
					bboxes[label] = r
				else:
					r = bboxes[label]
					x = r[0] + r[2] - 1
					y = r[1] + r[3] - 1
					if i < r[0]:
						r[0] = i 
					if i > x:
						x = i 
					if j < y:
						r[1] = j
					if j > y:
						y = j 
					r[2] = x - r[0] + 1
					r[3] = y - r[1] + 1
			i += 1
		j += 1
	return bboxes

# Se dibuja rectángulos a partir de cajas englobantes
def paint_rectangles(img, bboxes):
	for bbox in bboxes.values():
		start_point = (bbox[0], bbox[1])
		end_point = (bbox[0] + bbox[2], bbox[1] + bbox[3])
		img = cv2.rectangle(img, start_point, end_point, (0, 0, 255), 2)
			
	return img

# Parámetros a, b de la matriz de proyección
def find_translation(angle, height=0.15, radius=0):
	a = height * np.cos(angle) + radius * np.sin(angle)
	b = height * np.sin(angle) - radius * np.cos(angle)
	return a, b

# Matriz de proyección ad-hoc para el problema
def p_matrix(angle, height, alpha, radius=0):
    a, b = find_translation(angle, height, radius)
    p = np.array([[112 * np.cos(angle), -alpha, 112 * b],
                  [112 * np.cos(angle) - alpha * np.sin(angle), 0, a * alpha + 112 * b],
                  [np.cos(angle), 0, b]])
    return p

# Se obtiene la proyección de un punto en el eje y de la imagen
def find_yt_xproy(x, p_mat):
	point = np.array([x, 0, 1])
	(_, Y, w) = p_mat.dot(point)

	return int(Y / w)

# Parámetros simulados para la matriz RT e intrínseca
angle = 15 * np.pi / 180
height = 0.15
alpha = 150
radius = 0.09

# Matriz de proyección y homografía
p_mat = p_matrix(angle, height, alpha, radius)
h_mat = np.linalg.inv(p_mat) * np.linalg.det(p_mat)

# Selección de imágenes para inferencia y proyección
imgs_path = ['ssl-dataset-master/1_resized/{:05d}.jpg'.format(i) for i in range(817, 864 + 1)]
# imgs_path = ['ssl-dataset-master/1_resized/{:05d}.jpg'.format(i) for i in range(865, 930 + 1)]

# Proyección de líneas de distancia frontales al robot
lines = np.zeros(cv2.imread(imgs_path[0]).shape, dtype=np.uint8)
x_dist = [0.25, 0.3, 0.35, 0.4, 0.45, 0.5] # metros
for x in x_dist:
	yp = find_yt_xproy(x, p_mat)
	lines = cv2.line(lines, (0, yp), (223, yp), (255, 0, 0), 1)

x_dist = [0.6, 0.75, 1.0, 1.5, 2.0] # metros
for x in x_dist:
	yp = find_yt_xproy(x, p_mat)
	lines = cv2.line(lines, (0, yp), (223, yp), (0, 0, 255), 1)

# Se itera sobre el set de imágenes seleccionado
for i, f in enumerate(imgs_path):
	img = cv2.imread(f)

	# Detección de la pelota a través de V-U
	yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
	vu = (yuv[:, :, 2].astype(np.float32)) - yuv[:, :, 1].astype(np.float32)
	vu[vu < 0.0] = 0.0
	vu[vu > 255.0] = 255.0
	vu = vu.astype(np.uint8)
	_, vu_mask = cv2.threshold(vu, 30, 255, cv2.THRESH_BINARY)	

	# Se obtienen contornos
	(contours, hierarchy) = cv2.findContours(vu_mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
		
	# Se obtienen componentes conectados y cajas englobantes
	dest = np.zeros(vu_mask.shape, np.uint8)
	if len(contours) > 0:
		cont = max(contours, key=lambda x: cv2.contourArea(x))
		dest = cv2.drawContours(dest, [cont], -1, (255), thickness=cv2.FILLED)

		num_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(dest)
		bbox = get_blobs(labels)

		dest = cv2.cvtColor(dest, cv2.COLOR_GRAY2BGR)

		# Podría ser más flexible. Por ahora se acepta solo 1 pelota
		ball_pos = int(bbox[1][0] + bbox[1][2] / 2), int(bbox[1][1] + bbox[1][3])
		cv2.circle(img, ball_pos, 2, (0, 0, 255), -1)

		# Distancia del robot a la pelota
		pball = np.array([ball_pos[0], ball_pos[1], 1])
		(xt, yt, t) = h_mat.dot(pball)
		print('pixel pos: {}, pos r/robot: ({:.2f}, {:.2f})'.format(ball_pos, xt / t, yt / t))

		# Se dibuja caja englobante en dest
		paint_rectangles(dest, bbox)

	# Se muestran distintas vistas
	fstack = np.hstack((cv2.bitwise_or(img, lines), img))

	cv2.imshow('Ball test', fstack)
	cv2.imshow('Img', cv2.bitwise_or(img, lines))
	cv2.imshow('V-U', cv2.bitwise_and(vu_mask, lines[:, :, 0] + lines[:, :, 2]))

	# Salir si k es q o Esc. Guardar frames si k es s
	k = cv2.waitKey() & 0xFF
	if k == ord('q') or k == 27:
		break
	if k == ord('s'):
		print('asd')
		cv2.imwrite('pres/final/{}_ball_bbox.png'.format(i), dest)
		cv2.imwrite('pres/final/{}_img.png'.format(i), img)
		cv2.imwrite('pres/final/{}_proy_lines.png'.format(i), cv2.bitwise_or(img, lines))

cv2.destroyAllWindows()