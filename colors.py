import os
import cv2
import numpy as np

imgs_path = ['ssl-dataset-master/1_resized/{:05d}.jpg'.format(i) for i in range(817, 864 + 1)]
# imgs_path = ['ssl-dataset-master/1_resized/{:05d}.jpg'.format(i) for i in range(865, 930 + 1)]

# Máscara de 3 canales a partir de un canal
def create_3channel_mask(src_mask, shape, color):
	dst_mask = np.zeros(shape, dtype=np.uint8)
	dst_mask[:, :, :] = color

	dst_mask = cv2.bitwise_and(dst_mask, dst_mask, mask=src_mask)

	return dst_mask

# Se itera sobre el set de imágenes seleccionado
for i, f in enumerate(imgs_path):
	img = cv2.imread(f)

	# Se obtienen distintos mapas de color. Se trabaja solo con CIELab
	img_plot = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
	hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

	cielab = cv2.cvtColor(img, cv2.COLOR_BGR2Lab)
	cielab = cv2.GaussianBlur(cielab, (9, 9), 0)
    
	# Se separan canales. split() también sirve
	L = cielab[:, :, 0]
	a = cielab[:, :, 1]
	b = cielab[:, :, 2]

	# Se transforman los canales a float para obtener un ángulo de dirección en el plano ab. No se usa finalmente
	af = cielab[:, :, 1].astype(float) - 127
	bf = cielab[:, :, 2].astype(float) - 127
	#h = (np.arctan(np.divide(bf, af, out=np.ones_like(a) * 1e9, where=a!=0)) * 255 / np.pi + 127).astype(np.uint8)

	# Se determinan rangos de colores de forma empírica. puede afinarse
	# Máscara de pelota
	ball_mask = cv2.inRange(cielab, (0, 125, 100), (190, 180, 180))
	kernel = np.ones((6, 6), dtype=np.uint8)
	ball_mask = cv2.morphologyEx(ball_mask, cv2.MORPH_CLOSE, kernel, iterations=1)

	# Máscara del campo
	field_mask = cv2.inRange(cielab, (0, 0, 135), (120, 112, 150))
	kernel = np.ones((4, 4), dtype=np.uint8)
	field_mask = cv2.dilate(field_mask, kernel)
	field_mask = cv2.morphologyEx(field_mask, cv2.MORPH_CLOSE, kernel, iterations=1)

	# Máscara de líneas
	line_mask = cv2.inRange(cielab, (120, 0, 130), (150, 115, 145))
	kernel = np.ones((9, 9), dtype=np.uint8)
	line_mask = cv2.morphologyEx(line_mask, cv2.MORPH_CLOSE, kernel, iterations=3)

	# RGB
	orange = (0, 110, 255)
	green = (0, 200, 0)
	white = (230, 230, 230)

	# Se muestra pelota filtrada bajo este método
	cv2.imshow('cielab_ball', create_3channel_mask(ball_mask, img.shape, orange)) # pelota
	
	# Se pintan las regiones enmascaradas.
	img_masked = cv2.addWeighted(img, 1, create_3channel_mask(line_mask, img.shape, white), 0.8, 0)
	img_masked = cv2.addWeighted(img_masked, 1, create_3channel_mask(field_mask, img.shape, green), 0.8, 0)
	img_masked = cv2.addWeighted(img_masked, 1, create_3channel_mask(ball_mask, img.shape, orange), 0.8, 0)
	cv2.imshow('Img masked', img_masked)

	# Salir si k es q o Esc. Guardar frames si k es s
	k = cv2.waitKey() & 0xFF
	if k == ord('q') or k == 27:
		break
	if k == ord('s'):
		cv2.imwrite('pres/final/{}_color_seg.png'.format(i), img_masked)

cv2.destroyAllWindows()