# Proyecto IPD-441
Este repositorio contiene los archivos para el entrenamiento de un modelo de inferencia para detección de objetos usando EfficientDet-D0 y la API de [Object Detection] de Tensorflow2.
Es necesario instalar TensorFlow2 seguido de la API en cuestión:

```
pip install tensorflow pycocotools
cd models/research
protoc object_detection/protos/*.proto --python_out=.
cp object_detection/packages/tf2/setup.py .
python -m pip install --use-feature=2020-resolver .
```

En caso de utilizar una GPU para el entrenamiento del modelo referirse a la instalación de CUDA y otras dependencias ([tf-gpu]).
Para verificar que todo está correctamente instalado ejecutar
```
python object_detection/builders/model_builder_tf2_test.py
```

Los programas empleados para la tarea de Benchmarking y extensión del algoritmo para lo obtención de resultados vistos en el informe son:

- **TrainModel.ipynb**: *Transfer learning* para el modelo EfficientDet-D0 a partir del dataset [SSL Robocup], obtención del modelo en su versión Lite y métricas de desempeño *Average Precision* (AP) y *Average Recall* (AR). Información sobre las celdas de código y su implementación se encuentra descrita en el notebook.
- **Inference.ipynb**: Tarea de inferencia para la detección de objetos a partir del modelo entrenado con el notebook TrainModel.ipynb. Información sobre las celdas de código y su implementación se encuentra descrita en el notebook.
- **colors.py**: Script utilizado para la segmentación de elementos en la cancha a partir del mapa de color CIELab.
- **proy_lines.py**: Script que muestra proyección de líneas horizontales frente al robot a partir de la transformación de geometría proyectiva presentada en este trabajo. Incorpora también la segmentación de la pelota y la posición de la misma a través de la matriz de homografía.
- **detect_and_project.py**: Algoritmo final. Incluye la inferencia continua de los frames seleccionados, detección de posición de arco y pelota en píxeles y, la proyección de ellos en el plano de ejes coordenados locales del robot.

Directorios y su contenido:

- **inference_graph**: modelo exportado usando la API de Object Detection una vez realizado el entrenamiento.
- **models**: copia del repositorio de la API de Object Detection.
- **ssl-dataset-master**: copia del repositorio del dataset [SSL Robocup].
- **training** contiene el checkpoint de pesos preentrenados, el dataset en formato .tfrecord y los checkpoints generados durante el entrenamiento.


[tf-gpu]: https://www.tensorflow.org/install/gpu#install_cuda_with_apt
[SSL Robocup]: https://github.com/bebetocf/ssl-dataset
[Object Detection]: https://github.com/tensorflow/models
