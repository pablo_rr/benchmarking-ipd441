
robocup - v3 2021-07-06 2:48pm
==============================

This dataset was exported via roboflow.ai on July 6, 2021 at 7:05 PM GMT

It includes 931 images.
Robocup are annotated in Tensorflow TFRecord (raccoon) format.

The following pre-processing was applied to each image:
* Auto-orientation of pixel data (with EXIF-orientation stripping)
* Resize to 416x416 (Stretch)

No image augmentation techniques were applied.


